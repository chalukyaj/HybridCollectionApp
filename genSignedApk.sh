#!/bin/sh
rm -f platforms/android/build/outputs/apk/armv7/release/android-armv7-release-unsigned-aligned.apk
rm -f platforms/android/build/outputs/apk/armv7/release/android-armv7-release-signed.apk

 ~/Android/Sdk/build-tools/27.0.3/zipalign -v -p 4 platforms/android/build/outputs/apk/armv7/release/android-armv7-release-unsigned.apk platforms/android/build/outputs/apk/armv7/release/android-armv7-release-unsigned-aligned.apk

~/Android/Sdk/build-tools/27.0.3/apksigner sign --ks colAppReleaseKey.jks --out platforms/android/build/outputs/apk/armv7/release/android-armv7-release-signed.apk platforms/android/build/outputs/apk/armv7/release/android-armv7-release-unsigned-aligned.apk

~/Android/Sdk/build-tools/27.0.3/apksigner verify platforms/android/build/outputs/apk/armv7/release/android-armv7-release-signed.apk

cp platforms/android/build/outputs/apk/armv7/release/android-armv7-release-signed.apk signedApk.apk

