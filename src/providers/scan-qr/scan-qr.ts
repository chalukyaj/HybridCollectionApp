import { BarcodeScanner ,BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Events } from 'ionic-angular';
import {Injectable} from "@angular/core";

/*
  Generated class for the ScanQrProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ScanQrProvider {
    options :BarcodeScannerOptions = {
        formats : 'QR_CODE'
    };

    constructor(
        private events: Events,
        private qrCodeScanner: BarcodeScanner
    ) {
        
    }
    scanQR(uID:string, mandatoryFields: any = []) {
        this.options.prompt = "Scan The QR of Agent";
        
        this.qrCodeScanner.scan(this.options).then((qrCodeData) => {
            let err = '';
            
            if(qrCodeData.cancelled) {
                err = 'Scanning Cancelled';
            }
            else if(qrCodeData.format !== 'QR_CODE') {
                err = 'Please Scan a Valid QR Code';
            }
            else {
                let qrObj:any = {};
                try {
                    qrObj = JSON.parse(qrCodeData.text);
                    mandatoryFields.forEach(field => {
                        if(typeof (qrObj[field]) === 'undefined') {
                            err = 'Please Scan a Valid Code';
                            return false;
                        }
                    })
                }
                catch (e) {
                    err = 'Please Scan a Valid Code';
                }

                if(!err) {
                    this.events.publish('scanComplete' + uID, true, qrObj);
                    return;
                }
            }
            this.events.publish('scanComplete' + uID, false, qrCodeData);
        }, (err) => {
            this.events.publish('scanComplete' + uID, false);
            if(err === 'Illegal access') {
                err = 'Please allow CAMERA access for the Shift Handover Function To Work';
            }
            else {
                err = 'An ERROR occurred while trying to open Camera. Please report this to \'support@stanplus.co.in\'';
            }
            this.events.publish('scanComplete' + uID, false);
        });   
    }
}
