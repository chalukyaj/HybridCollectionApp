import { Injectable } from '@angular/core';

import { Events, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


// import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { Http } from '@angular/http';

@Injectable()
export class UserData {
    _favorites: string[] = [];
    HAS_LOGGED_IN = 'hasLoggedIn';
    FAVOURITES = 'favourites';
    HAS_SEEN_TUTORIAL = 'hasSeenTutorial';
    fleetURL = "https://stanfleet.com/";
    _loginID: string = '';
    _userName: string = '';
    _colID: string = '';
    _hasSeenTutorial: boolean = false;
    _hasLoggedIn: boolean = false;
    _userHash: string = '';
    _collectionAgent = false;
    _loading: boolean = false;
    _initCount: number = 0;
    _loadingObj: any = null;
    publishLogin: boolean = true
    userInfo: any = {};

    constructor(
        public events: Events,
        public storage: Storage,
        private loadCtrl: LoadingController,
        private alertCtrl: AlertController,
        private http: Http
    ) {
        this._initCount = 0;
        this.initialize();
    }

    loadingStatus() {
        return this._loading;
    }

    showLoading(opts: any = false) {
        if(!this._loading) {
            this._loading = true;
            if(!opts) {
                opts = {
                    content: 'Please Wait...'
                };
            }

            this._loadingObj = this.loadCtrl.create(opts);
            this._loadingObj.present();
        }
    }

    hideLoading() {
        if(this._loading) {
            this._loading = false;
            this._loadingObj.dismiss();
        }
    }

    finalizeIntialize() {
        this._initCount++;
        if(this._initCount >= 6) {
            //Dismiss
            this.events.publish('user:initialized');
        }
    }

    initialize() {
        this._initCount = 0;
        this.userInfo = {};
        //Show
        this._collectionAgent = false;
        this.storage.get(this.FAVOURITES).then(favs => {
            if(favs) {
                this._favorites = JSON.parse(favs);
            }
            this.finalizeIntialize();
        });

        this.getColID().then(colID => {
            if(colID) {
                this._hasLoggedIn = true;
                this._colID = colID;
            }
            this.finalizeIntialize();
        });

        this.getUserHash().then(hash => {
            if(hash) {
                this._userHash = hash;
            }
            this.finalizeIntialize();
        });

        this.getUsername().then(name => {
            if(name) {
                this._userName = name;
            }
            this.finalizeIntialize();
        });

        this.getIsCollectionAgent().then(collectionAgent => {
            if(collectionAgent) {
                this._collectionAgent = true;
            }
            else {
                this._collectionAgent = false;
            }
            this.finalizeIntialize();
        });
        
        this.storage.get(this.HAS_SEEN_TUTORIAL).then((hasSeenTutorial) => {
            this._hasSeenTutorial = hasSeenTutorial ? true: false;
            this.finalizeIntialize();
        });
    }

    hasFavorite(colID: string): boolean {
        return (this._favorites.indexOf(colID) > -1);
    };

    addFavorite(colID: string): void {
        this._favorites.push(colID);
        this.storage.set(this.FAVOURITES, JSON.stringify(this._favorites));
    };

    removeFavorite(colID: string): void {
        let index = this._favorites.indexOf(colID);
        if (index > -1) {
            this._favorites.splice(index, 1);
            this.storage.set(this.FAVOURITES, JSON.stringify(this._favorites));
        }
    };
    
    setIsCollectionAgent(colAgent: boolean): void {
        this._collectionAgent = colAgent;
        this.storage.set('collectionAgent', colAgent);
    };

    getIsCollectionAgent(): Promise<string> {
        return this.storage.get('collectionAgent');
    };

    validateLoginData (data: any) {
        this.hideLoading();
        data = data.json();
        if(typeof(data.status) !== 'undefined' && data.status && typeof (data.CollectionID) !== 'undefined' && data.CollectionID) {
            this._hasLoggedIn = true;
            this.storage.set(this.HAS_LOGGED_IN, true);
            this.setUsername(data.Name);
            this.setColID(data.CollectionID);
            this.setUserHash(data.UserHash);
            this.setIsCollectionAgent(data.ApprovedCA ? true : false);
            this.storage.set(this.HAS_SEEN_TUTORIAL, true);
            this._hasSeenTutorial = true;
            let loginAlert = this.alertCtrl.create({
                message : 'Login Information was validated for <b>' + (this._collectionAgent ? 'COLLECTION AGENT' : 'KIOSK AGENT') + '</b>',
                title: 'Login Successful'
            });
            
            loginAlert.present();
            
            setTimeout(() => {
                loginAlert.dismiss();
            }, 2000);
            this.userInfo = {};
            
            if(this.publishLogin) {
                this.events.publish('user:login');
            }
        }
        else {

            let loginAlert = this.alertCtrl.create({
                message : 'Your credentials didn\'t match any in our database. Please check the information you\'ve entered',
                title: 'Login Failed'
            });
            
            loginAlert.present();
            
            setTimeout(() => {
                loginAlert.dismiss();
            }, 2000);
        }

        return data;
    }

    login(login: any, callBack : any = () => {}, errCallBack : any = () => {}, noLoading:boolean = false, publishLogin:boolean = true): any {
        
        this.showLoading({
            content: 'Please wait while we verify the Login Information from our Servers'
        });
        this.loginUser('TestController/androidLogin', callBack, {
            userid : login.username,
            pwd : login.password
        }, this.validateLoginData, errCallBack, noLoading, publishLogin);
    };

    signup(username: string): void {
        console.log(username);
        // this.storage.set(this.HAS_LOGGED_IN, true);
        // this.setUsername(username);
        // console.log(this.tst);
        // this.events.publish('user:signup');
    };

    processLogout(publish:boolean = true) {
        this.storage.remove(this.HAS_LOGGED_IN);
        this._hasLoggedIn = false;
        this.storage.remove('username');
        this._userName = '';

        this.storage.remove('userHash');
        this._userHash = '';
        this.storage.remove('colID');
        this._colID = '';
        this.storage.remove(this.HAS_SEEN_TUTORIAL);
        this._hasSeenTutorial = false;
        this.userInfo = {};
        if(publish) {
            this.events.publish('user:logout');
        }
    }

    handleShiftHandover() {

    }

    logout(): void {
        this.processLogout();
    };

    setColID(colID: string): void {
        this._colID = colID;
        this.storage.set('colID', colID);
    };

    getColID(): Promise<string> {
        return this.storage.get('colID');
    };

    setUserHash(userHash: string): void {
        this._userHash = userHash;
        this.storage.set('userHash', userHash);
    };

    getUserHash(): Promise<string> {
        return this.storage.get('userHash');
    };

    setUsername(username: string): void {
        this._userName = username;
        this.storage.set('username', username);
    };

    getUsername(): Promise<string> {
        return this.storage.get('username').then((value) => {
            return value;
        });
    };

    setLoginID(id: string): void {
        this._loginID = id;
        this.storage.set('loginID', id);
    };

    getLoginID(): Promise<string> {
        return this.storage.get('loginID');
    };

    hasLoggedIn(): Promise<boolean> {
        return this.storage.get(this.HAS_LOGGED_IN);
    };

    checkHasSeenTutorial(): Promise<string> {
        return this.storage.get(this.HAS_SEEN_TUTORIAL).then((value) => {
            return value;
        });
    };


    showErrMsg(msg:string, ttl:string = 'Alert', tOut: number = 4000) {
        let alert = this.alertCtrl.create({
            message: msg,
            title: ttl
        });

        alert.present();

        setTimeout(() => {
            alert.dismiss();
        }, tOut);
    }

    processJSON(data: any) {
        this.hideLoading();
        let json: any = {success: false, error: 'Server Responded with an Invalid Format'};

        try {
            json = data.json()
        }
        catch(e) {
            
        }

        return json;
    }

    loginUser(url:string, callBack:any, params:any = {}, mapFunction:any = this.processJSON, errCallBack: any = () => {}, noLoading: boolean = false, publishLogin: boolean = true) {
        this.publishLogin = publishLogin;
        this.http.post(this.fleetURL + url, params)
        .map(mapFunction, this)
        .subscribe(
            callBack,
            error => {
                errCallBack(error);

                let msg = '', support = ' Please report this at support@stanplus.co.in';
                switch(error.status) {
                    case 0 :
                        msg = 'Can\'t talk to our Servers. Please check your Mobile Data or WIFI or report this at support@stanplus.co.in';
                    break;

                    case 404 : 
                        msg = `The Reqested URL Couldn\'t be found.${support}`;
                    break;

                    case 403 : 
                        msg = 'User Not Authorized. Please logout and try again';
                    break;

                    case 500 : 
                        msg = `A Server Error Occurred.${support}`;
                    break;
                }

                this.showErrMsg(msg, 'Error');
                if(!noLoading) {
                    this.hideLoading();
                }
            },
            () => {
                if(!noLoading) {
                    this.hideLoading();
                }
            }
        );

        return;
    }
}
