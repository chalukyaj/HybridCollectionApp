import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { UserData } from './user-data';
import { Observable } from 'rxjs/Observable';
//import { WelcomePage } from '../pages/welcome/welcome';
//import { NavController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { AlertController } from 'ionic-angular';

@Injectable()

export class CollectionData {
    value: any;
    data: any;
    colIds: any;
    loadCount: number = 0;
    baseURL: string = '';

    constructor(public http: Http, 
        public user: UserData,
        private alertCtrl: AlertController
    ) {
        this.baseURL = this.user.fleetURL;
    }

    load(): any {
        if (this.data) {
            return Observable.of(this.data);
        } 
        else {
            return this.http.get('assets/data/data.json')
                .map(this.processData, this);
        }
    }

    showErrMsg(msg:string, ttl:string = 'Alert', tOut: number = 4000) {
        let alert = this.alertCtrl.create({
            message: msg,
            title: ttl
        });

        alert.present();

        setTimeout(() => {
            alert.dismiss();
        }, tOut);
    }

    request(url:string, callBack:any, params:any = {}, mapFunction:any = this.processJSON, errCallBack: any = () => {}) {
        if(!this.user._hasLoggedIn) {
            return null;
        }

        params.loginID = this.user._colID;
        params.userHash = this.user._userHash;

        this.http.post(this.baseURL + url, params)
        .map(mapFunction, this)
        .subscribe(
            callBack,
            error => {
                errCallBack(error);
                console.log(error);
                let msg = '', support = ' Please report this at support@stanplus.co.in';
                switch(error.status) {
                    case 0 :
                        msg = 'Can\'t talk to our Servers. Please check your Mobile Data or WIFI or report this at support@stanplus.co.in';
                    break;

                    case 404 : 
                        msg = `The Reqested URL Couldn\'t be found.${support}`;
                    break;

                    case 403 : 
                        msg = 'User Not Authorized. Please logout and try again';
                    break;

                    case 500 : 
                        msg = `A Server Error Occurred.${support}`;
                    break;
                }

                this.showErrMsg(msg, 'Error');
                this.user.hideLoading();
            },
            () => {
                this.user.hideLoading();
            }
        );

        return;
    }

    fetchColData(params: any = {}, callBack: any = () => {}, errCallBack: any = () => {}): any {

        if(!this.user._hasLoggedIn) {
            return null;
        }

        this.user.showLoading({
            content: 'Please wait while we fetch data...'
        });

        this.request('FinanceOps/getCollectionAmounts/', callBack, params, this.processCollectionData, errCallBack);
    }

    fetchColDetails(params: any = {}, callBack: any= null, errCallBack: any = () => {}): any {

        if(!this.user._hasLoggedIn) {
            return null;
        }

        this.user.showLoading({
            content: 'Please wait while we fetch data...'
        });
        
        this.request('FinanceOps/getCollectionAmounts/', callBack, params, this.processJSON, errCallBack);
    }

    processJSON(data: any) {
        this.user.hideLoading();
        let json: any = {success: false, error: 'Server Responded with an Invalid Format'};

        try {
            json = data.json()
        }
        catch(e) {
            
        }

        return json;
    }

    processCollectionData(data: any) {
        let colAmts:any = null;
        
        try {
            colAmts = data.json();
        }
        catch (e) {
            colAmts = {success: false, data: 'Server responded with invalid format'};
        }

        if(typeof(colAmts.success) !== 'undefined' && colAmts.success) {
            colAmts = colAmts.data;
            colAmts.info = {};
            let balances = colAmts.balances;
            let owners = colAmts.owners;
            let agents = colAmts.agents;

            let agentsC = {};
            let ownersC = {};

            agents.forEach(details => {
                agentsC[details.id] = {
                    name: details.Name,
                    mob: details.Mobile
                }
            });

            owners.forEach(details => {
                ownersC[details.oCID] = {
                    name: details.cName,
                    mob: details.ph1
                }
            });

            owners = colAmts.owners = ownersC;
            agents = colAmts.agents = agentsC;
            balances.forEach(element => {
                let key = element.id;
                if(key.substr(3,2) === 'CA') {
                    colAmts.info[key] = typeof(agents[key]) !== 'undefined' ? agents[key] : {name: 'NA', mob: 'NA'};
                }
                else {
                    colAmts.info[key] = typeof(owners[key]) !== 'undefined' ? owners[key] : {name: 'NA', mob: 'NA'};
                }
            });
        }
        else {
            colAmts.balances = [];
            colAmts.agents = colAmts.owners = {};
        }

        this.user.hideLoading();
        return colAmts;
    }

    getCollectionData(params: any = {}): any {
        return this.fetchColData(params);
    }

    processData(data: any) {
        this.user.hideLoading();
        this.data = data.json();

        return this.data;
    }

    getTimeline(dayIndex: number, queryText = '', excludeTracks: any[] = [], segment = 'all') {
        return this.load().map((data: any) => {
            let day = data.schedule[dayIndex];
            day.fetchedCount = 0;

            queryText = queryText.toLowerCase().replace(/,|\.|-/g, ' ');
            let queryWords = queryText.split(' ').filter(w => !!w.trim().length);

            day.groups.forEach((group: any) => {
                group.hide = true;

                group.sessions.forEach((session: any) => {
                    // check if this session should show or not
                    this.filterSession(session, queryWords, excludeTracks, segment);

                    if (!session.hide) {
                        // if this session is not hidden then this group should show
                        group.hide = false;
                        day.fetchedCount++;
                    }
                });
            });

            return day;
        });
    }

    filterSession(session: any, queryWords: string[], excludeTracks: any[], segment: string) {

        let matchesQueryText = false;
        if (queryWords.length) {
            // of any query word is in the session name than it passes the query test
            queryWords.forEach((queryWord: string) => {
                if (session.name.toLowerCase().indexOf(queryWord) > -1) {
                    matchesQueryText = true;
                }
            });
        } else {
            // if there are no query words then this session passes the query test
            matchesQueryText = true;
        }

        // if any of the sessions tracks are not in the
        // exclude tracks then this session passes the track test
        let matchesTracks = false;
        session.tracks.forEach((trackName: string) => {
            if (excludeTracks.indexOf(trackName) === -1) {
                matchesTracks = true;
            }
        });

        // if the segement is 'favorites', but session is not a user favorite
        // then this session does not pass the segment test
        let matchesSegment = false;
        if (segment === 'favorites') {
            if (this.user.hasFavorite(session.name)) {
                matchesSegment = true;
            }
        } else {
            matchesSegment = true;
        }

        // all tests must be true if it should not be hidden
        session.hide = !(matchesQueryText && matchesTracks && matchesSegment);
    }

    getSpeakers() {
        return this.load().map((data: any) => {
            return data.speakers.sort((a: any, b: any) => {
                let aName = a.name.split(' ').pop();
                let bName = b.name.split(' ').pop();
                return aName.localeCompare(bName);
            });
        });
    }

    getTracks() {
        return this.load().map((data: any) => data.tracks.sort());
    }

    getMap() {
        return this.load().map((data: any) => {
            return data.map
        });
    }
}
