import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { ConferenceApp } from './app.component';
import { ColViewPage } from '../pages/col-view/col-view';
import { AboutPage } from '../pages/about/about';
import { PopoverPage } from '../pages/about-popover/about-popover';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { CollectionAmounts } from '../pages/collection-amounts/collection-amounts';
import { CollectionFilterPage } from '../pages/collection-filter/collection-filter';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SupportPage } from '../pages/support/support';
import { ColDetailsPage } from '../pages/col-details/col-details';
import { CollectionData } from '../providers/collection-data';
import { UserData } from '../providers/user-data';
import { WelcomePage } from '../pages/welcome/welcome';
import { CallNumber } from '@ionic-native/call-number';
import { Toast } from '@ionic-native/toast';
import { ShiftHandoverPage } from '../pages/shift-handover/shift-handover';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ScanQrProvider } from '../providers/scan-qr/scan-qr';
import { Brightness } from '@ionic-native/brightness';
import { Dialogs } from '@ionic-native/dialogs';

@NgModule({
    declarations: [
        ConferenceApp,
        AboutPage,
        AccountPage,
        LoginPage,
        MapPage,
        PopoverPage,
        CollectionAmounts,
        CollectionFilterPage,
        SignupPage,
        TabsPage,
        TutorialPage,
        SupportPage,
        WelcomePage,
        ShiftHandoverPage,
        ColDetailsPage,
        ColViewPage
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicModule.forRoot(ConferenceApp, {}, {
            links: [
                { component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
                { component: CollectionAmounts, name: 'CollectionAmounts', segment: 'collection-amounts' },
                { component: CollectionFilterPage, name: 'ScheduleFilter', segment: 'scheduleFilter' },
                { component: MapPage, name: 'Map', segment: 'map' },
                { component: AboutPage, name: 'About', segment: 'about' },
                { component: TutorialPage, name: 'Tutorial', segment: 'tutorial' },
                { component: SupportPage, name: 'SupportPage', segment: 'support' },
                { component: LoginPage, name: 'LoginPage', segment: 'login' },
                { component: AccountPage, name: 'AccountPage', segment: 'account' },
                { component: SignupPage, name: 'SignupPage', segment: 'signup' },
                { component: WelcomePage, name: 'WelcomePage', segment: 'welcome' },
                { component: ColDetailsPage, name: 'ColDetailsPage', segment: 'colDetails' },
                { component: ColViewPage, name: 'ColViewPage', segment: 'colView' }
            ]
        }),
        IonicStorageModule.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        ConferenceApp,
        AboutPage,
        AccountPage,
        LoginPage,
        MapPage,
        PopoverPage,
        CollectionAmounts,
        CollectionFilterPage,
        SignupPage,
        TabsPage,
        ShiftHandoverPage,
        TutorialPage,
        SupportPage,
        WelcomePage,
        ColDetailsPage,
        ColViewPage
    ],
    providers: [
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        CollectionData,
        UserData,
        InAppBrowser,
        SplashScreen,
        CallNumber,
        Toast,
        BarcodeScanner,
        ScanQrProvider,
        Brightness,
        Dialogs
    ]
})
export class AppModule { }
