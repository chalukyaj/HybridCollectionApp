import { Component, ViewChild } from '@angular/core';

import { Events, MenuController, Nav, Platform, AlertController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Storage } from '@ionic/storage';

//import { AboutPage } from '../pages/about/about';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
//import { MapPage } from '../pages/map/map';
import { SignupPage } from '../pages/signup/signup';
//import { TutorialPage } from '../pages/tutorial/tutorial';
import { CollectionAmounts } from '../pages/collection-amounts/collection-amounts';
import { SupportPage } from '../pages/support/support';
import { WelcomePage } from '../pages/welcome/welcome';
import { ShiftHandoverPage } from '../pages/shift-handover/shift-handover';
import { CollectionData } from '../providers/collection-data';
import { UserData } from '../providers/user-data';
import { ColViewPage } from '../pages/col-view/col-view';
import { ColDetailsPage } from '../pages/col-details/col-details';
//import { AppUpdate } from '@ionic-native/app-update';

export interface PageInterface {
    title: string;
    name: string;
    component: any;
    icon: string;
    logsOut?: boolean;
    index?: number;
    tabName?: string;
    tabComponent?: any;
    approvedCA?: boolean
//    appUpdate?: boolean
}

@Component({
    templateUrl: 'app.template.html'
})
export class ConferenceApp {
    // the root nav is a child of the root app component
    // @ViewChild(Nav) gets a reference to the app's root nav
    @ViewChild(Nav) nav: Nav;

    approvedCA: boolean = false;
    // List of pages that can be navigated to from the left menu
    // the left menu only works after login
    // the logTabsPagein page disables the left menu
    appPages: PageInterface[] = [
        
    ];
    loggedInPages: PageInterface[] = [
        { title: 'Collection Amounts', name: 'CollectionAmounts', component: CollectionAmounts, icon: 'calendar', approvedCA: true },
        // { title: 'Shift Handover', name: 'TabsPage', component: TabsPage, tabComponent: ShiftHandoverPage, index: 0, icon: 'swap' },
        // { title: 'Collection View', name: 'TabsPage', component: TabsPage, tabComponent: ColViewPage, index: 1, icon: 'cash'},

        { title: 'Shift Handover', name: 'ShiftHandoverPage', component: ShiftHandoverPage, icon: 'swap' },
        { title: 'My Balances', name: 'ColDetailsPage', component: ColDetailsPage, icon: 'paper'},
        { title: 'Collection Approval', name: 'ColViewPage', component: ColViewPage, icon: 'cash' },
        //{ title: 'Map View', name: 'MapPage', component: MapPage, icon: 'map' },
        //{ title: 'About', name: 'AboutPage', component: AboutPage, icon: 'information-circle' },
        { title: 'Account', name: 'AccountPage', component: AccountPage, icon: 'person' },
        { title: 'Support', name: 'SupportPage', component: SupportPage, icon: 'help'},
        { title: 'Logout', name: '', component: null, icon: 'log-out', logsOut: true },
//        { title: 'Check Version Update', name: '', component: null, icon: 'md-cloud-download', appUpdate: true }
    ];
    loggedOutPages: PageInterface[] = [
        { title: 'Welcome Page', name: 'WelcomePage', component: WelcomePage, icon: 'help-buoy'},
        { title: 'Login', name: 'LoginPage', component: LoginPage, icon: 'log-in' },
        { title: 'Support', name: 'SupportPage', component: SupportPage, icon: 'help' },
        { title: 'Signup', name: 'SignupPage', component: SignupPage, icon: 'person-add' }
    ];
    rootPage: any;

    constructor(
        public events: Events,
        public userData: UserData,
        public menu: MenuController,
        public platform: Platform,
        public confData: CollectionData,
        public storage: Storage,
        public splashScreen: SplashScreen,
        private alertCtrl: AlertController
        //private appUpdate: AppUpdate
    ) {        
        this.rootPage = null;
        this.approvedCA = false;
        // Check if the user has already seen the tutorial
        this.events.subscribe('user:initialized', () => {
            this.processUser();
        });

        // decide which menu items should be hidden by current login status stored in local storage
        this.listenToLoginEvents();
    }

    processUser() {
        //if (this.userData._hasSeenTutorial) {
            if(this.userData._hasLoggedIn) {
                this.approvedCA = this.userData._collectionAgent;
                if(this.approvedCA) {
                    this.rootPage = CollectionAmounts;
                }
                else {
                    this.rootPage = ShiftHandoverPage;
                }
            }
            else {
                this.rootPage = WelcomePage;
            }
            this.enableMenu(this.userData._hasLoggedIn);
        // } else {
        //     this.rootPage = TutorialPage;
        // }
        this.platformReady()
    }

    ionViewDidEnter() {
        this.processUser();
    }

    openPage(page: PageInterface) {
        let params = {};

        // the nav component was found using @ViewChild(Nav)
        // setRoot on the nav to remove previous pages and only have this page
        // we wouldn't want the back button to show in this scenario
        if (page.index) {
            params = { tabIndex: page.index };
        }

        // If we are already on tabs just change the selected tab
        // don't setRoot again, this maintains the history stack of the
        // tabs even if changing them from the menu


        if (page.logsOut === true) {
            // Give the menu time to close before changing to logged out
            this.alertCtrl.create({
                message: 'Do you want to <b>Handover</b> your shift\'s collection now ?',
                title: "Shift Handover",
                cssClass: 'editColAmtAlert',
                buttons: [
                    {
                        text: 'LOGOUT WITHOUT HANDOVER',
                        cssClass: 'editColCancel',
                        handler: () => {
                            this.userData.logout();
                        }
                    },
                    {
                        text: 'YES - Go to Shift Handover',
                        cssClass: 'editColConfirm',
                        handler: () => {
                            this.nav.setRoot(ShiftHandoverPage);
                        }
                    }
                ]
            }).present();
    
            return;
        }
        // else if (page.appUpdate) {
        //     const updateUrl = this.userData.fleetURL + 'update.xml';
        //     this.appUpdate.checkAppUpdate(updateUrl);
        // }
        
        if (this.nav.getActiveChildNavs().length && page.index != undefined) {
            this.nav.getActiveChildNavs()[0].select(page.index);
        } 
        else {
            // Set the root of the nav with params if it's a tab index
            this.nav.setRoot(page.component, params).catch((err: any) => {
                console.log(`Didn't set nav root: ${err}`);
            });
        }
    }

    // openTutorial() {
    //     this.nav.setRoot(TutorialPage);
    // }

    listenToLoginEvents() {
        this.events.subscribe('user:login', () => {
            if(this.userData._hasLoggedIn) {
                this.approvedCA = this.userData._collectionAgent;
                
                if(this.approvedCA) {
                    this.nav.setRoot(CollectionAmounts);
                    this.rootPage = CollectionAmounts;
                }
                else {
                    this.nav.setRoot(ShiftHandoverPage);
                    this.rootPage = ShiftHandoverPage;
                }
            }
            else {
                this.rootPage = WelcomePage;
            }
            this.enableMenu(this.userData._hasLoggedIn);
        });

        this.events.subscribe('user:signup', () => {
            this.enableMenu(true);
        });

        this.events.subscribe('user:logout', () => {
            this.nav.setRoot(WelcomePage);
            this.approvedCA = false;
            this.enableMenu(false);
        });
    }

    enableMenu(loggedIn: boolean) {
        this.menu.enable(loggedIn, 'loggedInMenu');
        this.menu.enable(!loggedIn, 'loggedOutMenu');
    }

    platformReady() {
        // Call any initial plugins when ready
        this.platform.ready().then(() => {
            this.splashScreen.hide();
        });
    }

    isActive(page: PageInterface) {
        let childNav = this.nav.getActiveChildNavs()[0];

        // Tabs are a special case because they have their own navigation   
        if (childNav) {
            if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
                return 'primary';
            }
            return;
        }

        if (this.nav.getActive() && this.nav.getActive().name === page.name) {
            return 'primary';
        }
        return;
    }
}
