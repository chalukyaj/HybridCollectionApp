import { Component, ViewChild } from '@angular/core';

import { Events, MenuController, NavController, Slides } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { WelcomePage } from '../welcome/welcome';

import { UserData } from '../../providers/user-data';
import { CollectionAmounts } from '../collection-amounts/collection-amounts';
import { TabsPage } from '../tabs-page/tabs-page';


@Component({
    selector: 'page-tutorial',
    templateUrl: 'tutorial.html'
})

export class TutorialPage {
    showSkip = true;

    @ViewChild('slides') slides: Slides;

    constructor(
        public navCtrl: NavController,
        public menu: MenuController,
        public storage: Storage,
        private events : Events,
        public user : UserData
    ) { }

    startApp() {
        let hasLoggedIn = this.user._hasLoggedIn;
        let viewPage:any = WelcomePage;

        if(hasLoggedIn) {
            if(this.user._collectionAgent) {
                viewPage = CollectionAmounts;
            }
            else {
                viewPage = TabsPage;
            }
        }

        this.storage.set('hasSeenTutorial', 'true');
        this.user._hasSeenTutorial = true;
        this.navCtrl.setRoot(viewPage);
        this.events.publish('user:initialized');
    }

    onSlideChangeStart(slider: Slides) {
        this.showSkip = !slider.isEnd();
    }

    ionViewWillEnter() {
        this.slides.update();
    }

    ionViewDidEnter() {
        // the root left menu should be disabled on the tutorial page
        this.menu.enable(false);
    }

    ionViewDidLeave() {
        // enable the root left menu when leaving the tutorial page
        this.menu.enable(true);
    }
}
