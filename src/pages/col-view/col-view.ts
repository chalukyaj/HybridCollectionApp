import { Component } from '@angular/core';
import { Toast } from '@ionic-native/toast';
import { NavController, NavParams, ToastController, ViewController, AlertController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { ScanQrProvider } from '../../providers/scan-qr/scan-qr';
import { WelcomePage } from '../welcome/welcome';
import { Brightness } from '@ionic-native/brightness';
import { Http } from '@angular/http';
import { Dialogs } from '@ionic-native/dialogs';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Component({
    selector: 'page-col-view',
    templateUrl: 'col-view.html',
})
export class ColViewPage {
    payer: string = '';
    amount: number = 0;
    qrLink: string = '';
    loginID: string = '';
    amtData: any = false;
    baseURL: string = '';
    userHash: string = '';
    showQR: boolean = false;
    scanProg: boolean = false;
    loading: boolean = false;
    colReceive: boolean = false;
    scannedRec: any = false;
    countDown : any = null;
    confirmPayAmt: boolean = false;
    otpRec: string = '';
    defaultBrightness:any = -1;
    networkReq:any = null;
    submitted:boolean = false;
    otpSwitch:boolean = false;
    qrLinkParams: any = {};
    otpDetails: any = {};
    agtPatt = new RegExp(/^[a-zA-Z]{3}(CA)[0-9]+$/);
    isAgent: boolean = true;

    constructor(
        public tst: Toast,
        public http: Http,
        public user: UserData,
        public navParams: NavParams,
        public events: Events,
        public navCtrl: NavController,
        private scanQR: ScanQrProvider,
        public viewCtrl : ViewController,
        public toastCtrl: ToastController,
        public brightness: Brightness,
        public dialog: Dialogs,
        public alertCtrl: AlertController
    ) {
        this.baseURL = this.user.fleetURL; 
    }

	dismiss() {
		if(!this.submitted) {
			this.viewCtrl.dismiss(false);
		}
	}

    ionViewDidEnter() {
        this.otpDetails = {
            receiver : '',
            payer: '',
            amount: '',
            otp : ''
        };
        
        this.payer = this.navParams.get('payer');
        this.isAgent = this.agtPatt.test(this.payer);
        this.amtData = this.navParams.get('amtData');
        this.submitted = false;
        this.amount = 0;
        this.qrLink = '';
        this.loginID = '';
        this.userHash = '';
        this.showQR = false;
        this.scanProg = false;
        this.loading = false;
        this.colReceive = false;
        this.scannedRec = false;
        this.confirmPayAmt = false;
        this.otpSwitch = false;
        this.qrLinkParams = {};
        
        if(this.user._hasLoggedIn) {
            this.loginID = this.user._colID;
            
            this.userHash = this.user._userHash;
            if(this.payer) {
                this.amount = this.amtData.amt;
                this.colReceive = true;
                this.alertCtrl.create({
                    message: 'You are about to receive <b>Rs. '+(this.amount)+'</b><br><br>Please wait while we generated the QR Code from Server for continuing the process',
                    title: 'Collection Initiated'
                }).present();
                console.log([this, this.isAgent]);
                if(!this.isAgent) {
                    return false;
                }
                let data = {
                    payer: this.payer,
                    amount: this.amount
                };

                this.networkReq = this.syncServer('otpCol', data);
                this.networkReq.subscribe(res => {
                    this.networkReq = false;
                    if(res.success && res.data && res.data.otp) {
                        let expiry = res.data.exp*1000;

                        let tOut = expiry - Date.now();

                        let dataRec = {
                            recName : encodeURI(this.user._userName),
                            receiver : this.loginID,
                            payer: this.payer,
                            amount: this.amount,
                            otp : res.data.otp,
                            validTill : expiry,
                            tOut : tOut
                        };

                        this.qrLinkParams = dataRec;
                        this.genQRLink();
                        
                        this.countDown = setInterval(() => {
                            if(this.qrLinkParams && this.qrLinkParams.tOut >= 1000) {
                                this.qrLinkParams.tOut -= 1000;
                            }
                            else {
                                this.loading = this.showQR = false;
                                this.qrLink = '';
                                this.qrLinkParams = {};
                                this.alertCtrl.create({
                                    title: 'QR Expired',
                                    message: 'The QR code has expired. Please regenerate the QR to continue'
                                }).present();
                                clearInterval(this.countDown);
                                this.countDown = null;
                            }
                        }, 1000);

                        this.otpRec = res.data.otp;
                    }
                    else {
                        this.loading = this.showQR = false;
                        this.dialog.alert('Error : ' + res.data);
                    }
                });
            }
        }
        else {
            this.loginID = '';
            this.navCtrl.push(WelcomePage);
        }
    }

    ionViewDidLeave() {
        if(this.networkReq) {
            this.networkReq.unsubscribe();
        }

        this.user.hideLoading();
        if(this.countDown) {
            clearInterval(this.countDown);
            this.countDown = null;
        }
        this.loading = this.showQR = false;
    }

    toggleOtp() {
        this.otpSwitch = !this.otpSwitch;

        if(!this.qrLink) {
            this.genQRLink();
        }

        if(this.scanProg) {
            this.validateCol();
        }
    }

    genQRLink() {
        if(!this.otpSwitch && this.qrLinkParams) {
            let qrData = JSON.stringify(this.qrLinkParams).replace(new RegExp('"', 'g'), '%22'); 

            this.qrLink = 'https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='+qrData+'&choe=UTF-8';
    
            // this.defaultBrightness = this.brightness.getBrightness().then((brightVal:any)  => {
            //     this.defaultBrightness = brightVal;
            //     //this.brightness.setBrightness(0.1);
            // });
    
            //this.brightness.setBrightness(1);
    
            this.loading = false;
        }
    }

    syncServer (type:string, data:any = {}):any {
        let url:string = this.baseURL + 'TwoWayAuth/';
        switch(type) {
            case 'otpCol' : 
                url += 'getOtpCol';
                this.showQR = this.loading = true;
            break;

            case 'validateCol' : 
                url += type;
            break;

            case 'confirmCol' : 
                url = this.baseURL + 'FinanceOps/processCollections/';
            break;
        }

        data.loginID = this.loginID;
        data.userHash = this.userHash;
        this.user.showLoading();
        return this.http.post(url, data).map(this.processJson, this);
    }

    processJson (data:any):any {
        this.user.hideLoading();
        return data.json();
    }

    showLoadingToast () {
        this.dialog.alert('Please wait while we contact the server!');
    }

    showQRMsg () {
        this.alertCtrl.create({
            message: 'Show this QR to the KIOSK Agent for Approval',
            title: 'Approve Transaction'
        }).present();
    }

    processColScan(qrData: any, now: any= 0) {
        this.amount = qrData.amount;
        this.scanProg = false;
        this.scannedRec = qrData;

        this.scannedRec.validTill;
        this.scannedRec.tOut = this.scannedRec.validTill - now;
        this.countDown = setInterval(() => {
            if(this.scannedRec && this.scannedRec.tOut >= 1000) {
                this.scannedRec.tOut -= 1000;
            }
            else {
                this.scanProg = false;
                this.scannedRec = false;
                clearInterval(this.countDown);
                this.countDown = null;
            }
        }, 1000);

        this.alertCtrl.create({
            message : 'Successful Scan. Please review the collection details and approve once you find them to be accurate',
            title: 'Success'
        }).present();
    }

    validateCol() {
        let uid:any = Date.now();
        if(this.countDown) {
            clearInterval(this.countDown);
            this.countDown = null;
        }

        this.scannedRec = false;
        this.scanProg = true;
        if(!this.otpSwitch) {
            this.scanQR.scanQR(uid, [
                'recName',
                'receiver',
                'payer',
                'amount',
                'otp',
                'validTill'
            ]);

            this.events.subscribe('scanComplete' + uid, (success: boolean, qrData: any = {}) => {
                let now = Date.now();
                if(success && qrData.payer === this.loginID && qrData.validTill > now) {
                    this.processColScan(qrData, now);
                }
                else {
                    this.scanProg = false;
                    this.scannedRec = false;

                    if(!success && qrData.cancelled) {
                        this.alertCtrl.create({
                            message : 'Please continue scanning the QR or enter the OTP manually to continue Collection Process',
                            title: 'Scanning Cancelled'
                        }).present();
                    }
                    else {
                        if(this.loginID !== qrData.payer) {
                            this.alertCtrl.create({
                                message : 'QR Code not generated for you',
                                title: 'Invalid Payer'
                            }).present();
                        }
                        else if(qrData.validTill <= now) {
                            this.alertCtrl.create({
                                message : 'QR Code Expired. Please ask Collection Agent to regenerate the Code',
                                title: 'QR Code Expired'
                            }).present();
                        }
                        else {
                            this.alertCtrl.create({
                                message: 'Please Scan a Valid Code',
                                title: 'Invalid QR Code'
                            }).present();
                        }
                    }
                }
            });
        }
    }

    confirmTransfer() {
        let data:any = {
            recID : this.scannedRec.receiver,
            amount: this.scannedRec.amount,
            otpRec: this.scannedRec.otp
        };

        if(!this.scannedRec.receiver && this.otpSwitch) {
            delete data.recID;
            data.otpMode = true;
        }

        this.syncServer('validateCol', data).subscribe(res => {
            let alert = null;
            if(res.success) {
                this.scanProg = false;
                this.scannedRec = false;
                alert = this.alertCtrl.create({
                    message: 'Please ask the Collection Agent to Proceed', 
                    title: 'Transaction Approved'
                });

                if(this.countDown) {
                    clearInterval(this.countDown);
                    this.countDown = null;
                }
                setTimeout(() => {
                    alert.dismiss();
                }, 10000);
            }
            else {
                alert = this.alertCtrl.create({
                    message: 'Transaction couldn\'t be approved. Please try again.<br><br>Error : <b>'+res.data+'</b>',
                    title: 'Approval Failed'
                });
                setTimeout(() => {
                    alert.dismiss();
                }, 5000);
            }
            alert.present();
        });
    }

    confirmCol() {
        this.amtData.list.from = this.payer;
        this.amtData.list.to = this.loginID;
        this.amtData.list.amt = this.amtData.amt;
        this.amtData.list.otp = this.otpRec;

        if(!this.agtPatt.test(this.amtData.list.from))
        {
            this.amtData.list.shift = true;
        }

        this.syncServer('confirmCol', this.amtData.list).subscribe(res => {
            let alert = null;

            if(res.success) {
                if(this.defaultBrightness >= 0) {
                    //this.brightness.setBrightness(this.defaultBrightness);
                }
                alert = this.alertCtrl.create({
                    message: 'Collection of <b>Rs. ' + this.amount + '</b> has been processed successfully',
                    title: 'Collection Successful'
                });

                if(this.countDown) {
                    clearInterval(this.countDown);
                    this.countDown = null;
                }

                this.viewCtrl.dismiss(true);
            }
            else {
                alert = this.alertCtrl.create({
                    message: 'Error : '+res.data, 
                    title: 'Collection Failed'
                });
            }

            alert.present();
        });
    }

    confirmOtpData() {
        if(this.otpDetails.amount <= 0) {
            let alert = this.alertCtrl.create({
                message: 'Invalid Amount Value',
                title: 'Invalid Data'
            });

            alert.present();

            setTimeout(() => {
                alert.dismiss();
            }, 2000);

            return false;
        }
        if(this.otpDetails.otp < 100000 || this.otpDetails.otp > 999999) {
            let alert = this.alertCtrl.create({
                message: 'Invalid OTP Value',
                title: 'Invalid Data'
            });

            alert.present();

            setTimeout(() => {
                alert.dismiss();
            }, 2000);

            return false;
        }

        this.scanProg = false;
        this.amount = this.otpDetails.amount;
        this.scannedRec = this.otpDetails;
    }
}