import { Component, ViewChild } from '@angular/core';

import { AlertController, App, Events, FabContainer, ItemSliding, List, ModalController, NavController, ToastController, LoadingController, Refresher } from 'ionic-angular';

/*
  To learn how to use third party libs in an
  Ionic app check out our docs here: http://ionicframework.com/docs/v2/resources/third-party-libs/
*/
// import moment from 'moment';

import { CollectionData } from '../../providers/collection-data';
import { UserData } from '../../providers/user-data';
//import { Storage } from "@ionic/storage";
import { CollectionFilterPage } from '../collection-filter/collection-filter';
import { CallNumber } from '@ionic-native/call-number';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { WelcomePage } from '../welcome/welcome';
import { ShiftHandoverPage } from '../shift-handover/shift-handover';
import { ColDetailsPage } from '../col-details/col-details';
import { ColViewPage } from '../col-view/col-view';

@Component({
    selector: 'collection-amounts',
    templateUrl: 'collection-amounts.html'
})
export class CollectionAmounts {
    // the list is a child of the schedule page
    // @ViewChild('collectionsList') gets a reference to the list
    // with the variable #collectionsList, `read: List` tells it to return
    // the List and not a reference to the element
    @ViewChild('collectionsList', { read: List }) collectionsList: List;

    loginID:string = '';
    dayIndex = 0;
    queryText = '';
    segment = 'all';
    excludeTracks: any = [];
    fetchedCount: any = [];
    groups: any = [];
    confDate: string;
    hasLoggedIn: any = false;
    colData: any = {'all' : {}, 'recents' : {}, 'favorites' : {}};
    prevPage:number = 0;sc
    prevParams:string = '';
    selfFetched = false;
    searchType:string = 'agt';
    curPage: number = 1;
    infEvent: any = null;
    REFRESH = 1;
    SEARCH = 2;
    FILTER = 3;
    PAGE = 4;

    constructor(
        public alertCtrl: AlertController,
        public app: App,
        public loadingCtrl: LoadingController,
        public modalCtrl: ModalController,
        public navCtrl: NavController,
        public toastCtrl: ToastController,
        public collectionData: CollectionData,
        public user: UserData,
        //public storage: Storage,
        public events: Events,
        private callNumber: CallNumber
    ) {
     }

    ionViewDidLoad() {
        this.app.setTitle('Collection Amounts');
        this.searchType = 'agt';
        this.hasLoggedIn = this.user._hasLoggedIn;
        
        if(this.hasLoggedIn && this.user._collectionAgent) {
            this.loginID = this.user._colID;
            this.updateAmounts();
        }
        else {
            this.loginID = '';
            this.navCtrl.setRoot(WelcomePage);
        }
    }

    switchSegment (type: number = 0, page: number = 1, event:any = null, params:any = {}) {
        this.events.publish('segmentChanged', this.segment);
        this.updateAmounts(type, page, event, params);
    }

    updateAmounts(type: number = 0, page: number = 1, event:any = null, params:any = {}) {
        if(!this.hasLoggedIn) {
            return false;
        }

        if(this.segment === 'favorites') {
            params.favList = [].concat(this.user._favorites);
            params.favList = params.favList.splice((page-1)*20, 20);
            if(!params.favList.length) {
                if(!this.user._favorites.length) {
                    let toast = this.toastCtrl.create({
                        message: 'No Favorites Added Yet !',
                        duration: 5000
                    });
                    toast.present();

                    this.events.subscribe('segmentChanged', () => {
                        toast.dismiss();
                    });
                }
                if(this.infEvent && type === this.PAGE) {
                    this.infEvent.enable(false);
                }
                return false;
            }
        }
        else if(this.segment === 'recents') {
            params.recents = true;
        }

        // Close any open sliding items when the collection amounts gets updated
        this.collectionsList && this.collectionsList.closeSlidingItems();

        if(this.searchType === 'bid') {
            params.bid = this.queryText;
            params.agt = '';
        }
        else {
            params.agt = this.queryText;
            params.bid = '';
        }

        let newParams = JSON.stringify([type, params]);
        if(type !== this.REFRESH && newParams === this.prevParams && page === this.prevPage ) {
            return null;
        }

        this.prevPage = page;
        params.id = this.loginID;

        params.pg = page;

        if(!this.selfFetched || type === this.REFRESH) {
            params.getSelf = true;
        }

        this.collectionData.fetchColData(params, (data) => {
            if(data.success === false) {
                let alert = this.alertCtrl.create({
                    message: `Error : ${data.data}`,
                    title : 'Error Occurred'
                });

                alert.present();

                setTimeout(() => {
                    alert.dismiss();
                }, 5000);
                if(event) {
                    if(type === this.REFRESH) {
                        event.complete();
                    }
                    else if(type === this.PAGE) {
                        event.enable(false);
                    }
                }
                return false;
            }
            if(!type || type == this.REFRESH || (type == this.SEARCH && newParams !== this.prevParams)) {
                this.curPage = 1;
                this.fetchedCount[this.segment] =data.balances.length;
                this.colData[this.segment] = data;
                if(this.infEvent) {
                    this.infEvent.enable(true);
                }
                if(event && type === this.REFRESH) {
                    event.complete();
                }
            }
            else if (type == this.PAGE || (type == this.SEARCH && newParams === this.prevParams)){
                this.curPage = page;
                let length = Object.keys(data.balances).length;
                this.fetchedCount[this.segment] += length;
    
                Object.assign(this.colData[this.segment].agents, data.agents);
                Object.assign(this.colData[this.segment].info, data.info);
                Object.assign(this.colData[this.segment].owners, data.owners);
                this.colData[this.segment].balances = this.colData[this.segment].balances.concat(data.balances);
                if(type === this.PAGE) {
                    this.infEvent = event;
                    if(length) {
                        event.complete();
                    }
                    else {
                        event.enable(false);
                    }
                }
            }
            if(params.getSelf) {
                this.user.userInfo = data.self;
            }
            this.prevParams = newParams;
            this.selfFetched = true;
        }, () => {
            if(event) {
                if(type === this.REFRESH) {
                    event.complete();
                }
                else if(type === this.PAGE) {
                    event.enable(false);
                }
            }
        });

        // this.collectionData.getTimeline(this.dayIndex, this.queryText, this.excludeTracks, this.segment).subscribe((data: any) => {
        //     //this.fetchedCount[this.segment] = data.fetchedCount[this.segment];
        //     this.groups = data.groups;
        // });
    }

    call(no: any) {
        this.callNumber.callNumber(no, true)
            .then(
                () => {
                }
            )
            .catch(
                () => {
                }
            );
    }

    presentFilter() {
        let modal = this.modalCtrl.create(CollectionFilterPage, this.excludeTracks);
        modal.present();

        modal.onWillDismiss((data: any[]) => {
            if (data) {
                this.excludeTracks = data;
                this.updateAmounts();
            }
        });
    }

    addFavorite(slidingItem: any, colID: string, colName: string) {

        if (this.user.hasFavorite(colID)) {
            // woops, they already favorited it! What shall we do!?
            // prompt them to remove it
            this.removeFavorite(slidingItem, colID, colName, 'Already added in Favorites');
        } else {
            // remember this ID as a user favorite
            this.user.addFavorite(colID);

            // create an alert instance
            let alert = this.alertCtrl.create({
                title: 'Favorite Added Successfully',
                message: '<b>' + colName + '</b> added to Favorites',
                buttons: [{
                    text: 'OK',
                    handler: () => {
                        // close the sliding item
                        slidingItem.close();
                    }
                }]
            });
            // now present the alert on top of all other content
            alert.present();
        }

    }

    removeFavorite(slidingItem: ItemSliding, colID: string, colName:string , title: string = 'Remove Favorite') {
        let alert = this.alertCtrl.create({
            title: title,
            message: 'Would you like to remove <b>' + colName + '</b> from your favorites?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        // they clicked the cancel button, do not remove the 
                        // close the sliding item and hide the option buttons
                        slidingItem.close();
                    }
                },
                {
                    text: 'Remove',
                    handler: () => {
                        // they want to remove this ID from their favorites
                        this.user.removeFavorite(colID);
                        this.updateAmounts();

                        // close the sliding item and hide the option buttons
                        slidingItem.close();
                    }
                }
            ]
        });
        // now present the alert on top of all other content
        alert.present();
    }

    openSocial(network: string, fab: FabContainer) {
        let loading = this.loadingCtrl.create({
            content: `Posting to ${network}`,
            duration: (Math.random() * 1000) + 500
        });
        loading.onWillDismiss(() => {
            fab.close();
        });
        loading.present();
    }

    doRefresh(refresher: Refresher) {
        this.updateAmounts(this.REFRESH, 1, refresher);
    }

    startCollection(payer:string) {
        this.navCtrl.push(ShiftHandoverPage, {
            payer: payer
        });
    }

    showCollectionList(payerData: any) {
        let payer = payerData.id;
        let payerName = this.colData[this.segment].info[payer].name;
        let colDetailsModal = this.modalCtrl.create(ColDetailsPage, {payer: payer, payerName: payerName, payerBal: payerData.bal});
        colDetailsModal.present();

        colDetailsModal.onDidDismiss((colDetails) => {
            if(colDetails) {
                let colViewModal = this.modalCtrl.create(ColViewPage, {
                    payer : payer,
                    amtData : colDetails
                });
                colViewModal.present();
                
                colViewModal.onDidDismiss((retVal) => {
                    if(retVal) {
                        this.updateAmounts(this.REFRESH);
                    }
                });
            }
            else {
                this.alertCtrl.create({
                    message: 'You cancelled the Collection Process. Please choose a payer again to reinitialize the process.',
                    title: 'Cancelled'
                }).present();
            }
        });
    }
}