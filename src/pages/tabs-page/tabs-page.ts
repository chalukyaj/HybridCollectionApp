import { Component } from '@angular/core';

import { NavParams } from 'ionic-angular';

import { NavController } from 'ionic-angular';
// import { AboutPage } from '../about/about';
// import { MapPage } from '../map/map';
import { ColViewPage } from '../col-view/col-view';
import { Storage } from '@ionic/storage';
//import { WelcomePage } from '../welcome/welcome';
import { LoadingController } from 'ionic-angular';
import { ShiftHandoverPage } from '../shift-handover/shift-handover';
import { UserData } from '../../providers/user-data';

@Component({
    templateUrl: 'tabs-page.html'
})
export class TabsPage {
    // set the root pages for each tab
    tab1Root: any = ShiftHandoverPage;
    tab2Root: any = ColViewPage;
    // tab3Root: any = MapPage;
    // tab4Root: any = AboutPage;
    mySelectedIndex: number;
    HAS_LOGGED_IN = 'hasLoggedIn';

    constructor(
        navParams: NavParams,
        public user: UserData,
        public storage: Storage,
        public navCtrl: NavController,
        public loadingCtrl: LoadingController
    ) {
        this.mySelectedIndex = navParams.data.tabIndex || 0;
    }

    ionViewDidLoad() {
    }
}
