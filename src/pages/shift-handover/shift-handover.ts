import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { CollectionData } from '../../providers/collection-data';
import { Toast } from '@ionic-native/toast';
import { Events } from 'ionic-angular';
// import { Http } from '@angular/http';
import { WelcomePage } from '../welcome/welcome';
import { ColDetailsPage } from '../col-details/col-details';

// import { Observable } from 'rxjs/Observable';
import { UserData } from '../../providers/user-data';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

//import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the ScanQrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    templateUrl: 'shift-handover.html',
})
export class ShiftHandoverPage {
    loginID:string = '';
    userHash:string = '';
    shiftData:any = null;
    baseURL: string = '';
    colDetailsModal: any = null;
    
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public collectionData: CollectionData,
        private tst: Toast,
        private events: Events,
        // private http: Http,
        private modalCtrl: ModalController,
        private alertCtrl: AlertController,
        private user: UserData
    ) {
        this.baseURL = this.user.fleetURL;
    }

    ionViewDidEnter() {
        this.colDetailsModal = null;

        this.loginID = this.user._colID;
        this.userHash = this.user._userHash;
        
        if(!this.loginID) {
            this.navCtrl.setRoot(WelcomePage);
        }
    }

    initiatePayment() {
        if(!this.colDetailsModal) {
            this.colDetailsModal = this.modalCtrl.create(ColDetailsPage, {
                payer: this.loginID,
                payerName: this.user._userName,
                payerBal: this.user.userInfo.bal
            });
        }

        this.colDetailsModal.present();

        this.colDetailsModal.onDidDismiss((colDetails) => {
            if(colDetails) {
                colDetails.list.from = this.user._colID;
                this.shiftData = colDetails;
                let loginSuccess = false;
                let logoutAlert = this.alertCtrl.create({
                    message: 'Please login to Approve the Shift Handover of <b>Rs. '+this.shiftData.amt+'</b>',
                    title: 'Shift Handover',
                    cssClass: 'editColAmtAlert',
                    enableBackdropDismiss: false,
                    inputs: [
                        {
                            name: 'userName',
                            type: 'number',
                            placeholder: 'Username'
                        },
                        {
                            name: 'password',
                            type: 'password',
                            placeholder: 'Password'
                        }
                    ],
                    buttons: [
                        {
                            text: 'CANCEL',
                            cssClass: 'editColCancel',
                            handler: () => {
                                if(loginSuccess) {
                                    this.events.publish('user:login');
                                    this.events.publish('user:initialized');
                                }
                            }
                        },
                        {
                            text: 'Log Out',
                            cssClass: 'editColCancel',
                            handler: () => {
                                this.user.processLogout();
                            }
                        },
                        {
                            text: 'LOGIN TO APPROVE HANDOVER OF Rs. '+this.shiftData.amt,
                            cssClass: 'editColConfirm',
                            handler: (userData) => {
                                //logoutAlert.dismiss();
                                let userName = userData.userName;
                                let password = userData.password;

                                if(!userName || userName.length !== 10) {
                                    this.tst.showShortCenter('Invalid UserName').subscribe();
                                }
                                else if(!password || password.length < 3) {
                                    this.tst.showShortCenter('Invalid Password').subscribe();
                                }
                                else {
                                    this.user.login({username : userName,password : password}, (data: any) => {
                                        if(data.status) {
                                            loginSuccess = true;
                                            this.shiftData.list.to = this.user._colID;

                                            if(this.shiftData.list.to === this.shiftData.list.from) {
                                                let alert = this.alertCtrl.create({
                                                    message: "Payer and Receiver have to be different. Please try a different login",
                                                    title: "Shift Handover Failed"
                                                });

                                                alert.present();
                                                setTimeout(() => {
                                                    alert.dismiss();
                                                }, 10000);
                                                
                                                return;
                                            }
                                            
                                            setTimeout(() => {
                                                this.user.showLoading({
                                                    content: 'Please wait while we process the shift handover'
                                                });
                                            }, 1000);
                                            
                                            this.shiftData.list.shift = true;

                                            this.collectionData.request('FinanceOps/processCollections/', (res) => {
                                                logoutAlert.dismiss();
                                                this.user.hideLoading();
                                                if(res.success) {
                                                    let alert = this.alertCtrl.create({
                                                        message: 'Shift Handover of <b>Rs. ' + this.shiftData.amt + '</b> was successfully processed',
                                                        title: "Shift Handover Successful"
                                                    });
                                                    alert.present();
                                                    setTimeout(() => {
                                                        alert.dismiss();
                                                    }, 25000);
                                                }
                                                else {
                                                    let alert = this.alertCtrl.create({
                                                        message: 'Something Went Wrong. Error : '+res.data,
                                                        title: "Shift Handover Failed"
                                                    });

                                                    alert.present();
                                                    setTimeout(() => {
                                                        alert.dismiss();
                                                    }, 15000);
                                                }
                                                this.events.publish('user:login');
                                                this.events.publish('user:initialized');
                                            }, this.shiftData.list);
                                        }
                                    }, ()=> {}, false, false);
                                }

                                return false;
                            }
                        }
                    ]
                });

                logoutAlert.present();
            }
            else {
                this.alertCtrl.create({
                    message: 'You cancelled the Collection Process. Please choose a payer again to reinitialize the process.',
                    title: 'Cancelled'
                }).present();
            }
        });
    }
}