import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController } from 'ionic-angular';

import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';

import { SignupPage } from '../signup/signup';
@Component({
    selector: 'page-user',
    templateUrl: 'login.html'
})
export class LoginPage {
    login: UserOptions = { username: '', password: '' };
    userPattern = /^([A-Za-z]+|[0-9]+)$/;

    constructor(
        public navCtrl: NavController, 
        public userData: UserData
    ) {
    }

    onLogin(form: NgForm) {
        if (form.valid) {
            this.userData.login(this.login);
        }
    }

    onSignup() {
        this.navCtrl.setRoot(SignupPage);
    }
}
