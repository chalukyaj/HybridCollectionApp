import {Component, ViewChild } from "@angular/core";
import { NavController, NavParams, ViewController, AlertController} from "ionic-angular";
import { Toast } from '@ionic-native/toast';
import { Content } from 'ionic-angular';
import { CollectionData } from '../../providers/collection-data';
import { UserData } from '../../providers/user-data';

/**
 * Generated class for the ColDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({selector: "page-col-details", templateUrl: "col-details.html"})
export class ColDetailsPage {
	@ViewChild(Content) content: Content;
	payer:string = '';
	loginID: string = '';
	colDetails:any = {};
	colDetailsFull: any = {};
	detailsCount:number = 0;
	bDetails:any = [];
	bDetailsFull:any = [];
	selectedAmt:number = 0;
	selectedList:any = {
		cAmts : {

		},
		to: '',
		from: ''
	};
	pageLimit = 20;
	REFRESH = 0;
	SEARCH = 1;
	PAGE = 2;
	curPage = 0;
	infEvent:any = null;
	pgLmt: number = 20;
	retCtrl:any = '';
	hasLoggedIn = this.user._hasLoggedIn;
	submitted: boolean = false;
	fifoAmt:any = '';
	fifoApplied: boolean = false;
	colFetch: any = null;
	payerName: string = '';
	showOwnInfo: boolean = false;
	scrollRef:any = {
		scrollPos: 0,
		start: 0,
		end: 9
	};
	
    constructor(
				public navCtrl : NavController, 
				public navParams : NavParams,
				public collectionData: CollectionData,
				public user: UserData,
				private tst: Toast,
				private alertCtrl: AlertController,
				//private events: Events,
				//private zone: NgZone,
				private viewCtrl: ViewController
		) {

    }

    ionViewDidLoad() {

		// this.content.ionScroll.subscribe(event => {
		// 	let nPos = (event.scrollTop/220)|0;
		// 	if(nPos === this.scrollRef.scrollPos) {
		// 		return;
		// 	}
		// 	this.scrollRef.scrollPos = nPos;
			
		// 	let start = 0, end = 0;

		// 	if(this.scrollRef.scrollPos < 3) {
		// 		start = 0;
		// 		end = 10;
		// 	}
		// 	else {
		// 		start = this.scrollRef.scrollPos - 3;
		// 		end = this.scrollRef.scrollPos + 6;
		// 	}
		// 	this.zone.run(() => {
		// 		this.bDetails = this.colDetails.bDetails.slice(start, end);
		// 		this.detailsCount = this.bDetails.length;
		// 		console.log([start, end, this.bDetails]);
		// 		console.log(this.scrollRef);
		// 	});
		// });

		this.fifoApplied = false;
		this.payer = this.navParams.get('payer');
		this.payerName = this.navParams.get('payerName');
		let balance = this.navParams.get('payerBal');
		
		this.colDetails = {};

		if(balance) {
			this.colDetails.balance = balance;
		}
		this.retCtrl = this.navParams.get('retCtrl');

		if(!this.payer) {
			this.showOwnInfo = true;
			this.payer = this.user._colID;
			this.payerName = this.user._userName;
			this.colDetails.balance = this.user.userInfo.bal;
		}
		else {
			this.showOwnInfo = false;
		}
		
		this.detailsCount = 0;
		this.bDetails = [];
		this.selectedAmt = 0;
		this.selectedList = {
			cAmts : {},
			to : '',
			from : ''
		};
		this.curPage = 0;
		this.fifoAmt = '';
		this.infEvent = null;
		this.pgLmt = 20;
		this.hasLoggedIn = this.user._hasLoggedIn;
		this.submitted = false;
		this.fetchColAmts();
	}
	
	dismiss() {
		if(!this.submitted) {
			if(this.colFetch && typeof(this.colFetch.unsubscribe) !== 'undefined') {
				this.colFetch.unsubscribe();
			}
			this.viewCtrl.dismiss(false);
		}
	}

	scrollToTop() {
		this.content.scrollToTop();
	}

	fetchColAmts (type:number = 0, page: number = 0, event:any = null) {
		if(type === this.REFRESH) {
			this.loginID = this.user._colID;
			let params:any = {};
			this.fifoAmt = '';
			this.fifoApplied = false;
			this.selectedAmt = 0;
			this.selectedList.cAmts = {};
			params.id = this.loginID;
			params.cid = this.payer;
			page = 0;
			//params.pg = page;
			this.collectionData.fetchColDetails(params, (res:any) => {
				this.colFetch = null;
				if(event) {
					event.complete();
				}
				if(this.infEvent) {
					this.infEvent.enable(true);
				}

				if(typeof(res.success) !== 'undefined' && res.success) {
					this.colDetails = res.data[this.payer];
					if(typeof(this.colDetails.bDetails) === 'undefined') {
						this.colDetails.bDetails = [];
						this.bDetails = [];
						this.detailsCount = 0;
					}
					else {
						this.bDetails = this.colDetails.bDetails.slice(0, this.pageLimit);
						this.detailsCount = this.bDetails.length;
					}
					
					// else if(type === this.PAGE) {
					// 	this.curPage = page;
					// 	this.bDetails = this.bDetails.concat(this.colDetails.bDetails);
					// 	this.detailsCount = this.bDetails.length;

					// 	if(type === this.PAGE) {
					// 		this.infEvent = event;

					// 		if(!this.colDetails.bDetails.length || this.colDetails.bDetails.length < this.pgLmt) {
					// 			event.enable(false);
					// 		}
					// 	}
					// }
				}
				else {
					this.detailsCount = 0;
					this.bDetails = [];
					return null;
				}
			},
			() => {
				this.detailsCount = 0;
				this.bDetails = [];
				return null;
			});
		}
		else if (type === this.PAGE) {
			let recCount = (page+1)*this.pageLimit;
			this.bDetails = this.colDetails.bDetails.slice(0, recCount);
			this.detailsCount = this.bDetails.length;
			this.infEvent = event;
			this.curPage = page;
			if(recCount === this.detailsCount) {
				event.complete();
			}
			else {
				event.enable(false);
			}
		}
	}

	toggleItem (index: number) {
		let colItem = this.bDetails[index];
		this.fifoApplied = false;
		this.fifoAmt = '';

		if(typeof(this.selectedList.cAmts[colItem.bid]) === 'undefined') {
			this.selectedList.cAmts[colItem.bid] = colItem.amt|0;
			this.selectedAmt += colItem.amt|0;
		}
		else {
			this.selectedAmt -= this.selectedList.cAmts[colItem.bid];
			delete this.selectedList.cAmts[colItem.bid];
		}
	}

	// trackChanges(index: any, colItem: any) {
	// 	return typeof(this.selectedList.cAmts[colItem.bid]) === 'undefined' ? false : true;
	// }
	
	submitAmts() {
		if(this.selectedAmt <= 0) {
			this.tst.show('Please select some amount to continue', '3000', 'center').subscribe();
			return false;
		}

		this.submitted = true;

		this.viewCtrl.dismiss({list: this.selectedList, amt: this.selectedAmt});
	}	

	changeAmtCollect(colItem: any, event: any) {
		event.stopPropagation();
		let bid = colItem.bid;
		var editVal = 'editAmtCol' + bid;
		let amt = (colItem.amt|0);

		let alert = this.alertCtrl.create({
			title: 'Edit Collectible Amount',
			message: 'Enter Amount to be Collected Against <b>' + bid + '</b> less than or equal to <b>' + amt + '</b>',
			cssClass: 'editColAmtAlert',
			inputs: [
				{
					name: editVal,
					type: 'number',
					value: (this.selectedList.cAmts[bid] ? this.selectedList.cAmts[bid] : amt),
					placeholder: 'Enter Amount to be Collected'
				}
			],
			buttons: [
				{
					text: 'Cancel',
					cssClass: 'editColCancel',
					handler: () => {
					}
				},
				{
					text: 'Confirm Amount',
					cssClass: 'editColConfirm',
					handler: (data) => {
						let editAmt = data[editVal];
						if(!editAmt || editAmt > amt) {
							this.tst.showShortTop('Amount Not Valid. Please Try Again').subscribe();
							return false;
						}
						else {
							this.fifoApplied = false;
							this.fifoAmt = '';
							this.selectedAmt -= this.selectedList.cAmts[bid];
							this.selectedList.cAmts[bid] = editAmt|0;
							this.selectedAmt += this.selectedList.cAmts[bid];
						}
					}
				}
			]
		});
	
		alert.present();
	}

	collectFIFO (amount: any) {
		this.selectedList = {
			cAmts : {},
			to : '',
			from : ''
		};
		
		let amt:number = 0;
		let BreakException = 0;
		try {
			this.colDetails.bDetails.forEach(colItem => {
				amt = colItem.amt|0;
				if(amount >= amt) {
					this.selectedList.cAmts[colItem.bid] = amt;
					amount -= amt;
					this.selectedAmt += amt;
				}
				else if(amount) {
					this.selectedList.cAmts[colItem.bid] = amount;
					this.selectedAmt += amount;
					throw BreakException;
				}
				
				if(!amount) {
					throw BreakException;
				}
			});
		}
		catch (e) {
		}
		this.fifoApplied = true;
	}

	selectViaFifo (amount:any = null) {
		let selectAll = false;

		if(amount === null) {
			amount = this.fifoAmt|0;
		}
		else {
			selectAll = true;
		}
		this.colDetails.balance |= 0;
		if(!amount || amount > this.colDetails.balance) {
			let alert = this.alertCtrl.create({
				title: 'Invalid Amt',
				message: 'Amount Not Valid. Please Try Again'
			});
		
			alert.present();
			return false;
		}

		if(amount === this.selectedAmt && this.fifoApplied) {
			return false;
		}

		if(this.selectedAmt && !selectAll) {
			let alert = this.alertCtrl.create({
				title: 'Confirm FIFO',
				message: 'Are you sure you want select using FIFO ?<br><b>NOTE :</b>This will unselect all the previous selections.',
				cssClass: 'editColAmtAlert',
				enableBackdropDismiss: false,
				buttons: [
					{
						text: 'Cancel',
						cssClass: 'editColCancel',
						handler: () => {
							alert.dismiss();
						}
					},
					{
						text: 'Confirm Amount',
						cssClass: 'editColConfirm',
						handler: () => {
							this.collectFIFO(amount);
							alert.dismiss();
						}
					}
				]
			});
		
			alert.present();
		}
		else {
			this.collectFIFO(amount);
		}
	}

	clearSelection() {
		this.fifoAmt = '';
		if(this.selectedAmt <= 0) {
			return false;
		}

		this.selectedAmt = 0;
		this.selectedList.cAmts = {};
	}
}